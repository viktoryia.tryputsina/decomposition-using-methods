package src.main.java.com.epam.decomposition;

public class Task3 {
    public static void main(String[] args) {
        int a = 92;
        int b = 46;
        int c = 23;
        int[] values = {a, b, c};

        int lsmMultiple = calculateLSMMultipleNumbers(values);

        System.out.println(lsmMultiple);
    }

    public static int calculateLSMMultipleNumbers(int[] values) {
        int currentLsm = calculateLSM(values[0], values[1]);

        for (int i = 2; i < values.length; i++) {
            currentLsm = calculateLSM(currentLsm, values[i]);
        }

        return currentLsm;
    }

    public static int calculateGCD(int a, int b) {
        int min = Math.min(a, b);
        int max = Math.max(a, b);
        int mod = max % min;

        while (mod != 0) {
            int temp = mod;
            mod = min % mod;
            min = temp;
        }

        return min;
    }

    public static int calculateLSM(int a, int b) {
        int gcd = calculateGCD(a, b);
        int lsm = a * b / gcd;
        return lsm;
    }
}
