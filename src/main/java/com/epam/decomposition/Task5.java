package src.main.java.com.epam.decomposition;

import java.util.Arrays;

public class Task5 {
    public static void main(String[] args) {
        int[] values1 = {2, 6, 90, 34, 75, 24, 6, 7, 36};
        double[] values2 = {2.56, 6.334, 90.7, 34.3, 75.88, 24.23, 6.78, 7.67, 36.32};

        int secondLargest1 = findSecondLargest(values1);
        double secondLargest2 = findSecondLargest(values2);

        System.out.println(secondLargest1);
        System.out.println(secondLargest2);
    }

    public static int findSecondLargest(int[] array) {
        Arrays.sort(array);
        return array[array.length - 2];
    }

    public static double findSecondLargest(double[] array) {
        Arrays.sort(array);
        return array[array.length - 2];
    }
}
