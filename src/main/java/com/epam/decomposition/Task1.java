package src.main.java.com.epam.decomposition;

public class Task1 {
    public static void main(String[] args) {
        int a = 154;
        int b = 44;

        int gcd = calculateGCD(a, b);
        int lsm = calculateLSM(a, b);

        System.out.println(gcd);
        System.out.println(lsm);
    }

    public static int calculateGCD(int a, int b) {
        int min = Math.min(a, b);
        int max = Math.max(a, b);
        int mod = max % min;

        while (mod != 0) {
            int temp = mod;
            mod = min % mod;
            min = temp;
        }

        return min;
    }

    public static int calculateLSM(int a, int b) {
        int gcd = calculateGCD(a, b);
        int lsm = a * b / gcd;
        return lsm;
    }
}
