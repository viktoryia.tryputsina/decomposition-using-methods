package src.main.java.com.epam.decomposition;

public class Task4 {
    public static void main(String[] args) {
        int sumOfFactorials = calculateSumOfFactorials();

        System.out.println(sumOfFactorials);
    }

    public static int calculateSumOfFactorials() {
        int result = 0;
        for (int i = 1; i <= 9; i++) {
            if (i % 2 != 0) {
                result += calculateFactorial(i);
            }
        }
        return result;
    }

    public static int calculateFactorial(int number) {
        if (number == 1) {
            return 1;
        } else {
            int result = number * calculateFactorial(number - 1);
            return result;
        }
    }
}
