package src.main.java.com.epam.decomposition;

public class Task2 {
    public static void main(String[] args) {
        int a = 92;
        int b = 46;
        int c = 23;
        int d = 115;
        int[] values = {a, b, c, d};

        int gcdMultiple = calculateGCDMultipleElements(values);

        System.out.println(gcdMultiple);
    }

    public static int calculateGCDMultipleElements(int[] values) {
        int currentGcd = calculateGCD(values[0], values[1]);

        for (int i = 2; i < values.length; i++) {
            currentGcd = calculateGCD(currentGcd, values[i]);
        }

        return currentGcd;
    }

    public static int calculateGCD(int a, int b) {
        int min = Math.min(a, b);
        int max = Math.max(a, b);
        int mod = max % min;

        while (mod != 0) {
            int temp = mod;
            mod = min % mod;
            min = temp;
        }

        return min;
    }
}
